﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Prototype
{
    public class SeeRose : IDeepClone<SeeRose>
    {
        private int dm;
        Graphics g;

        /// <summary>
        /// Color setzten / abgreifen
        /// </summary>
        public SolidBrush BlattFarbe { get; set; }
        public SolidBrush BluetenFarbe { get; set; }

        public int X { get; set; }
        public int Y { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="durchmesser"></param>
        /// <param name="blattFarbe"></param>
        /// <param name="bluettenFarbe"></param>
        /// <param name="x">fdfdfdf</param>
        /// <param name="y"></param>
        /// <param name="zeichenZiel"></param>
        public SeeRose(int durchmesser, Color blattFarbe, Color bluettenFarbe, int x, int y, Graphics zeichenZiel)
        {
            this.dm = durchmesser;
            this.BlattFarbe = new SolidBrush(blattFarbe);
            this.BluetenFarbe = new SolidBrush(bluettenFarbe);
            this.X = x;
            this.Y = y;
            this.g = zeichenZiel;
        }

        public void Draw()
        {
            g.FillEllipse(this.BlattFarbe, this.X - 5, this.Y - 5, this.dm, this.dm);
            g.FillEllipse(this.BluetenFarbe, this.X - 2, this.Y - 2, 4, 4);
        }

        public SeeRose Clone()
        {
            var x = this.MemberwiseClone() as SeeRose;

            x.BluetenFarbe = new SolidBrush(x.BluetenFarbe.Color);
            x.BlattFarbe = new SolidBrush(x.BlattFarbe.Color);

            return x;
        }
    }
}
