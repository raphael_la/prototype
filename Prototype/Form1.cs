﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prototype
{
    public partial class Form1 : Form
    {
        SeeRose prototype;
        Random r = new Random(DateTime.Now.Millisecond);

        public Form1()
        {
            InitializeComponent();
            prototype = new SeeRose(10, Color.Green, Color.Red, lastX, lastY, this.CreateGraphics());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for(int i = 0; i < 100000; i++)
            {
                var seerose = new SeeRose(10, Color.Green, Color.Red, 5, 5, this.CreateGraphics());
                //seerose.Draw();
            }
            sw.Stop();
            MessageBox.Show(sw.Elapsed.ToString());
            sw.Reset();

            sw.Start();
            var seerose2 = new SeeRose(10, Color.Green, Color.Red, 5, 5, this.CreateGraphics());
            for (int i = 0; i < 100000; i++)
            {
                var c = seerose2.Clone();
                //seerose.Draw();
            }
            sw.Stop();
            MessageBox.Show(sw.Elapsed.ToString());

            this.timer1.Enabled = true;
            this.timer1.Start();
        }

        int anzahl = 1;
        int lastX = 5;
        int lastY = 5;
        private void timer1_Tick(object sender, EventArgs e)
        {
            List<SeeRose> rosen = new List<SeeRose>();
            for (int i = 0; i < anzahl; i++)
            {
                SeeRose sr = prototype.Clone();
                if (r.Next(1, 40) % 5 == 0)
                    sr.BlattFarbe.Color = Color.GreenYellow;

                sr.X = lastX;
                sr.Y = lastY;
                rosen.Add(sr);
                lastX += 10;
                if (lastX > this.Width)
                {
                    lastX = 5;
                    lastY += 10;
                }  
            }

            anzahl *= 2;

            foreach (var r in rosen)
            {
                r.Draw();
            }
        }
    }
}
